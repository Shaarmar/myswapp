//
//  ViewController.swift
//  ContactCard
//
//  Created by Omar Shaarawi on 10/10/18.
//  Copyright © 2018 Omar Shaarawi. All rights reserved.
//

import Foundation
import PureLayout
import UIKit

class ViewController: UIViewController {
    var buttonIsSelected = false
    var buttonIsSelected2 = false
    
    let sixNintyFiveEast = ["6:47", "7:12", "7:17", "7:50", "8:05", "8:42", "9:15"] // 695 east
    let sixNintyEightEast = ["6:05", "6:20", "8:10", "9:05", "9:25", "9:48", "10:25", "11:25", "12:20", "13:25", "14:35", "15:15"] // 698 east
    
    let sixNintyEightWest = ["11:19", "12:19", "13:14", "13:44", "14:12", "14:40", "14:56", "15:11", "15:28", "17:32", "17:48", "18:26", "19:09", "20:24", "21:38"] // 698 west
    let sixNintyFiveWest = ["15:47", "16:15", "16:30", "16:51", "17:20", "18:15"] // 695 west
    
    func getDateAsString() -> String {
        let currentDateTime = Date()
        let formatter = DateFormatter()
        
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        
        let time = formatter.string(from: currentDateTime)
        let dateAsString = time
        return dateAsString
    }
    
    func getRealTime() -> String {
        let currentDateTime = Date()
        let formatter = DateFormatter()
        
        formatter.timeStyle = .short
        formatter.dateStyle = .none
        
        let time = formatter.string(from: currentDateTime)
        let dateAsString = time
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: dateAsString)
        
        dateFormatter.dateFormat = "HH:mm"
        let date24 = dateFormatter.string(from: date!)
        return date24
    }
    
    func stringToMinutes(input: String) -> Int {
        let components = input.components(separatedBy: ":")
        let hour = Int((components.first ?? "0")) ?? 0
        let minute = Int((components.last ?? "0")) ?? 0
        return hour * 60 + minute
    }
    
    func printArray(array: [String]) -> String {
        var str: [String] = []
        let timesMinutesArray: [Int] = array.map { stringToMinutes(input: $0) }
        for i in timesMinutesArray {
            let item = array[timesMinutesArray.index(of: i)!] //  "15:40"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "H:mm"
            let date12 = dateFormatter.date(from: item)!
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            str.append(date22)
        }
        return (str).joined(separator: "\n")
    }
    
    func bus(array: [String]) -> String? {
        let timesMinutesArray: [Int] = array.map { stringToMinutes(input: $0) }
        let dayMinute = stringToMinutes(input: getRealTime())
        let filteredTimesArray = timesMinutesArray.filter { $0 > dayMinute }
        if let firstTime = filteredTimesArray.first {
            let item = array[timesMinutesArray.index(of: firstTime)!] //  "15:40"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "H:mm"
            let date12 = dateFormatter.date(from: item)!
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            
            return date22
        }
        return "No Bus Available"
    }
    
    func bus2(array: [String]) -> String? {
        var str: [String] = []
        let timesMinutesArray: [Int] = array.map { stringToMinutes(input: $0) }
        let dayMinute = stringToMinutes(input: getRealTime())
        let filteredTimesArray = timesMinutesArray.filter { $0 > dayMinute }
        
        // let stringArray = filteredTimesArray.map { String($0) }
        for i in filteredTimesArray {
            let item = array[timesMinutesArray.index(of: i)!] //  "15:40"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "H:mm"
            let date12 = dateFormatter.date(from: item)!
            dateFormatter.dateFormat = "h:mm a"
            let date22 = dateFormatter.string(from: date12)
            str.append(date22)
        }
        if str.isEmpty {
            textLabel.text = "No Buses available"
            return "No Buses available"
        }
        else {
            return (str).joined(separator: "\n")
        }
    }
    
    func realS2M(str: String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a" // Your date format
        dateFormatter.timeZone = TimeZone.current // Current time zone
        if dateFormatter.date(from: str) != nil {
            let date = dateFormatter.date(from: str) // according to date format your date string
            let calendar = Calendar.current
            let comp = calendar.dateComponents([.hour, .minute], from: date!)
            let hour = comp.hour ?? 0
            let minute = comp.minute ?? 0
            let finalMinut: Int = (hour * 60) + minute
            return finalMinut
        }
        else {
            return 0
        }
    }
    
    func minutesToHoursMinutes(minutes: Int) -> (hours: Int, leftMinutes: Int) {
        return (minutes / 60, (minutes % 60))
    }
    
    lazy var upperView: UIView = {
        let view = UIView()
        view.autoSetDimension(.height, toSize: 128)
        view.backgroundColor = .white
        return view
    }()
    
    func addSubviews() {
        view.addSubview(upperView)
    }
    
    func setupConstraints() {
        upperView.autoPinEdge(toSuperviewEdge: .left)
        upperView.autoPinEdge(toSuperviewEdge: .right)
        upperView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .bottom)
    }
    
    lazy var segmentedControl: UISegmentedControl = {
        let control = UISegmentedControl(items: ["695 East", "698 East", "695 West", "698 West"])
        control.autoSetDimension(.height, toSize: 32.0)
        control.selectedSegmentIndex = 0
        control.layer.borderColor = UIColor.black.cgColor
        control.tintColor = UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0)
        control.addTarget(self, action: #selector(self.indexChanged(_:)), for: .valueChanged)
        return control
    }()
    
    lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    lazy var nextBusButton: UIButton = {
        let button = UIButton()
        button.setTitle("Next Bus", for: .normal)
        button.setTitleColor(UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0), for: .normal)
        button.layer.cornerRadius = 4.0
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 1.0
        button.tintColor = UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0)
        button.backgroundColor = .clear
        
        button.autoSetDimension(.width, toSize: 140.0)
        button.autoSetDimension(.height, toSize: 40.0)
        button.addTarget(self, action: #selector(self.btnClick(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(self.HoldDown(_:)), for: .touchDown)
        
        button.tag = 1
        return button
    }()
    
    lazy var nextBusTimesButton: UIButton = {
        let button = UIButton()
        button.setTitle("Current Buses", for: .normal)
        button.setTitleColor(UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0), for: .normal)
        button.layer.cornerRadius = 4.0
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 1.0
        button.tintColor = UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0)
        button.backgroundColor = .clear
        
        button.autoSetDimension(.width, toSize: 140.0)
        button.autoSetDimension(.height, toSize: 40.0)
        button.addTarget(self, action: #selector(self.btnClick(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(self.HoldDown(_:)), for: .touchDown)
        
        button.tag = 3
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = .white // UIColor(red:0.48, green:0.00, blue:0.10, alpha:1.0)
        addSubviews()
        // self.setupConstraints()
        // self.view.bringSubviewToFront(avatar)
        view.addSubview(segmentedControl)
        segmentedControl.autoPinEdge(toSuperviewEdge: .top, withInset: 60)
        segmentedControl.autoPinEdge(toSuperviewEdge: .left, withInset: 8.0)
        segmentedControl.autoPinEdge(toSuperviewEdge: .right, withInset: 8.0)
        segmentedControl.selectedSegmentIndex = UISegmentedControl.noSegment
        
        view.addSubview(nextBusButton)
        nextBusButton.autoPinEdge(toSuperviewEdge: .top, withInset: 120)
        nextBusButton.autoPinEdge(toSuperviewEdge: .right, withInset: 16.0)
        
        view.addSubview(nextBusTimesButton)
        nextBusTimesButton.autoPinEdge(toSuperviewEdge: .top, withInset: 120)
        nextBusTimesButton.autoPinEdge(toSuperviewEdge: .left, withInset: 16.0)
        
        view.addSubview(textLabel)
        textLabel.autoPinEdge(.top, to: .bottom, of: nextBusButton, withOffset: 4)
        
        let centreH = NSLayoutConstraint(item: textLabel, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 1)
        
        let centreV = NSLayoutConstraint(item: textLabel, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 1)
        
        view.addConstraint(centreH)
        view.addConstraint(centreV)
    }
    
    @objc func btnClick(_ sender: UIButton) {
        let btnsendtag: UIButton = sender
        btnsendtag.backgroundColor = .clear
        btnsendtag.setTitleColor(UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0), for: .normal)
        
        let tuple1 = minutesToHoursMinutes(minutes: realS2M(str: bus(array: sixNintyFiveEast)!) - realS2M(str: getDateAsString()))
        let tuple2 = minutesToHoursMinutes(minutes: realS2M(str: bus(array: sixNintyEightEast)!) - realS2M(str: getDateAsString()))
        let tuple3 = minutesToHoursMinutes(minutes: realS2M(str: bus(array: sixNintyFiveWest)!) - realS2M(str: getDateAsString()))
        let tuple4 = minutesToHoursMinutes(minutes: realS2M(str: bus(array: sixNintyEightWest)!) - realS2M(str: getDateAsString()))
        
        if btnsendtag.tag == 3 {
            print("Next Buses button")
            if segmentedControl.selectedSegmentIndex == 0 {
                print("1")
                if buttonIsSelected2 == true {
                    textLabel.text = bus2(array: sixNintyFiveEast)
                }
            }
            else if segmentedControl.selectedSegmentIndex == 1 {
                if buttonIsSelected2 == true {
                    textLabel.text = bus2(array: sixNintyEightEast)
                }
            }
            else if segmentedControl.selectedSegmentIndex == 2 {
                if buttonIsSelected2 == true {
                    textLabel.text = bus2(array: sixNintyFiveWest)
                }
            }
            else if segmentedControl.selectedSegmentIndex == 3 {
                if buttonIsSelected2 == true {
                    textLabel.text = bus2(array: sixNintyEightWest)
                }
            }
            else {
                textLabel.text = "No Buses available"
            }
            textLabel.setLineSpacing(lineSpacing: 10.0)
        }
        
        if btnsendtag.tag == 1 {
            if segmentedControl.selectedSegmentIndex == 0 {
                if buttonIsSelected == true {
                    if bus2(array: sixNintyFiveEast)!.contains(bus(array: sixNintyFiveEast)!) {
                        textLabel.text = "Next Bus At: \(bus(array: sixNintyFiveEast)!) \n\n\(tuple1.hours) Hour(s) and \(tuple1.leftMinutes) Minute(s) Aways" }
                    else {
                        textLabel.text = "No Bus available"
                    }
                }
            }
            else if segmentedControl.selectedSegmentIndex == 1 {
                print("2")
                if buttonIsSelected == true {
                    if bus2(array: sixNintyEightEast)!.contains(bus(array: sixNintyEightEast)!) {
                        textLabel.text = "Next Bus At: \(bus(array: sixNintyEightEast)!) \n\n\(tuple2.hours) Hour(s) and \(tuple2.leftMinutes) Minute(s) Aways" }
                    else {
                        textLabel.text = "No Bus available"
                    }
                }
            }
            else if segmentedControl.selectedSegmentIndex == 2 {
                print("3")
                if buttonIsSelected == true {
                    if bus2(array: sixNintyFiveWest)!.contains(bus(array: sixNintyFiveWest)!) {
                        textLabel.text = "Next Bus At: \(bus(array: sixNintyFiveWest)!) \n\n\(tuple3.hours) Hour(s) and \(tuple3.leftMinutes) Minute(s) Aways"
                    }
                    else {
                        textLabel.text = "No Bus available"
                    }
                }
            }
            else if segmentedControl.selectedSegmentIndex == 3 {
                print("4")
                if buttonIsSelected == true {
                    if bus2(array: sixNintyEightWest)!.contains(bus(array: sixNintyEightWest)!) {
                        textLabel.text = "Next Bus At: \(bus(array: sixNintyEightWest)!) \n\n\(tuple4.hours) Hour(s) and \(tuple4.leftMinutes) Minute(s) Aways"
                    }
                    else {
                        textLabel.text = "No Bus available"
                    }
                }
            }
            else {
                print("Nothing")
            }
        }
    }
    
    @objc func HoldDown(_ sender: UIButton) {
        let btnsendtag: UIButton = sender
        btnsendtag.backgroundColor = UIColor(red: 0.24, green: 0.65, blue: 0.23, alpha: 1.0)
        btnsendtag.setTitleColor(.white, for: .normal)
        
        buttonIsSelected = true
        buttonIsSelected2 = true
    }
    
    @objc func indexChanged(_ sender: UISegmentedControl) {
        textLabel.setLineSpacing(lineSpacing: 10.0)
        
        switch sender.selectedSegmentIndex {
        case 0:
            print("695 East")
            textLabel.text = printArray(array: sixNintyFiveEast)
        case 1:
            print("698 East")
            textLabel.text = printArray(array: sixNintyEightEast)
        case 2:
            print("695 West")
            textLabel.text = printArray(array: sixNintyFiveWest)
        case 3:
            print("698 West")
            textLabel.text = printArray(array: sixNintyEightWest)
        default:
            break
        }
        textLabel.setLineSpacing(lineSpacing: 10.0)
    }
}

extension UILabel {
    // Pass value for any one of both parameters and see result
    func setLineSpacing(lineSpacing: CGFloat = 0.0, lineHeightMultiple: CGFloat = 0.0) {
        guard let labelText = self.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        
        let attributedString: NSMutableAttributedString
        if let labelattributedText = self.attributedText {
            attributedString = NSMutableAttributedString(attributedString: labelattributedText)
        }
        else {
            attributedString = NSMutableAttributedString(string: labelText)
        }
        
        // Line spacing attribute
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, attributedString.length))
        
        attributedText = attributedString
    }
}
